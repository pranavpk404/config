# config

I have found this https://sourceforge.net/projects/arch-linux-gui/files/ it install arch linux with GUI my install.py is specifically made to tune that system

# Catppuccin
Wallpaper:https://github.com/catppuccin/wallpapers

GTK:https://github.com/catppuccin/gtk

Icons:https://www.xfce-look.org/p/1715570/

# Dracula
Wallpaper:https://github.com/aynp/dracula-wallpapers

GTK & Icon: https://draculatheme.com/gtk

Grub: https://draculatheme.com/grub

# Rose Pine
Wallpaper:https://gitlab.com/dwt1/wallpapers

GTK & Icons:https://github.com/rose-pine/gtk


# Nord
Wallpaper:https://github.com/midnitefox/Nord-Theme-Ports-and-Assets

GTK:https://github.com/EliverLara/Nordic

Icons:https://www.gnome-look.org/p/1360398/

# Gruvbox
Wallpaper:https://gitlab.com/exorcist365/wallpapers/

GTK:https://www.gnome-look.org/p/1681313

Icons:https://www.gnome-look.org/p/1681460

